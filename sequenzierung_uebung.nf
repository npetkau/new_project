nextflow.enable.dsl=2

process split_file {
  input:
    path infile
  output:
    path "${infile}.line*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
}

process count_start {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.startcount", emit: startcount
  script:
    """
    tail -n 1 ${infile} > sequenz
    grep -o -i "ATG" sequenz > grepresult
    cat grepresult | wc -l > ${infile}.startcount
    """
}

process count_stop {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.stopcount", emit: stopcount
  script:
    """
    tail -n 1 ${infile} > sequenz
    egrep -o -i 'TAA|TAG|TGA' sequenz > grepresult
    cat grepresult | wc -l >> ${infile}.stopcount
    """
}

process calculate_total_start {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "startcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > startcount
    """
}

process calculate_total_stop {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "stopcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > stopcount
    """
}

process out {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path starts
    path stops
  output:
    path "answer"
  script:
    """
    echo -n  "Die Anzahl der Startcodons beträgt: " > answer
    cat ${starts} >> answer
    echo -n  "Die Anzahl der Stopcodons beträgt: " >> answer
    cat ${stops} >> answer
    """
}


workflow {
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }
  inchannel = channel.fromPath(params.infile)
  splitfiles = split_file(inchannel)
  startcounts = count_start(splitfiles.flatten())
  stopcounts = count_stop(splitfiles.flatten())
  starts = calculate_total_start(startcounts.collect())
  stops = calculate_total_stop(stopcounts.collect())
  out(starts,stops)

}



